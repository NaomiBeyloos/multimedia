<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Naomi Beyloos">
    <meta name="keywords" content="Registreren">
    <meta name="date" content="20180517">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <title>Registreren</title>
</head>
<body>
        <header>

        <?php
            include('horizontal-nav.html');
        ?>
        
        <div class="banner">
        <h1>Registreren</h1>
        </div>
        
</header>
    
    <form method="post" action="welkom.php">
        <fieldset>
            <legend>Persoonlijke gegevens</legend>
        <div>
        <label for = "firstname">Voornaam</label>
        <input id = "firstname" type="text" name="firstname" maxlength = "20" required placeholder="Voornaam"/> 
        <!-- maxlength is aantal tekens dat gebruiker mag ingeven, required is not null, er moet een waarde in, placeholder daar geef je een tip mee -->
        <span>*</span>
        </div>
        
        <div>
        <label for = "lastname">Familienaam</label>
        
        <input id = "lastname" type="text" name="lastname" maxlength = "80" required placeholder="Familienaam"/> 
        <!-- maxlength is aantal tekens dat gebruiker mag ingeven, required is not null, er moet een waarde in, placeholder daar geef je een tip mee -->
        <span>*</span>
        </div>
        
        
        <div>
            <label for="comment">Commentaar</label>
            <textarea id = "comment" maxlength ="250" name = "comment"></textarea>
        </div>
        
        
        <div>
            <input name ="age-group" type = "radio" id ="child"><label for ="child">Kind</label>
            <input name ="age-group" type = "radio" id ="young adult"><label for ="young adult">Jong volwassene</label>
            <input name ="age-group" type = "radio" id ="adult"><label for ="adult">Volwassene</label>
            <!-- met iedereen eenzelfde name te geven kan er maar 1 bolletje aangeduid, radio button is voor 1 keuze, meerdere keuzes gebruik je selectievak checkbox -->
        </div>
        
        <div>
            <label for = "birthdate">Geboortedatum</label>
            <input id ="birthdate" type="date" name = "birthdate"/>

            
        </div>
        
        <div>
            <label for="email">E-mail</label>
            <input type="email" name="email" id="email" placeholder="naam@provider.be">
        </div>

        
         <div>
            <label for = "password">Wachtwoord</label>
            
            <input id ="password" type="password" name = "password" maxlength="40" required>
            <span>*</span>
        </div>
        
        </fieldset>
        
        <fieldset>
            <legend>Persoonlijke voorkeuren</legend>
        <div>
            <input type ="checkbox" name ="music" id="music"/><label for="music">Muziek</label>
            <input type ="checkbox" name ="books" id="books"/><label for="books">Boeken</label>
            <input type ="checkbox" name ="movies" id="movies"/><label for="movies">Films</label>
            <input type ="checkbox" name ="comics" id="comics"/><label for="comics">Strips</label>
            
        </div>
        
        <div>
            <label for="courses">Modules</label>
            <select id ="courses" name= "courses">
                <option value ="1">Multimedia</option>
                <option value ="50" selected>Programmeren2</option>
                <option value ="29">Databanken</option>
                 <!-- met selected achter de option value kies je er voor deze option te tonen -->
            </select>
        </div>
        
        <div>
            <label for="range">Bent u tevreden?</label>
            <input type="range" min="-4" max="2" step="2" value="-2" name="range" />
        </div>
        
        <div>
            <label for="uploadtask">Upload je taak in pdf-formaat</label>
            <input type="file" id="uploadtask" name="uploadtask"/>
        </div>
        
        </fieldset>
        <input type="submit" value="Verzenden"/>
        <p>Velden met een <span>*</span> zijn verplicht!</p>
    </form>
    
</body>
</html>