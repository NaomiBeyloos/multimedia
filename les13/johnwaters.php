<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <meta name="author" content="Naomi Beyloos">
    <meta name="keywords" content="John, Waters, Divine">
    <meta name="date" content="20180505">
    <title>John Waters</title>


</head>
<body>
 <!-- we voegen hier helemaal bovenaan ankers toe,
    waarnaar we springen als we op een item in het
    aside menu klikken, zodat de pagina niet meer naar
    bocenspringt! -->
    <span id="span1">John Waters</span>
    <span id="span2">Harris Glenn Milstead</span>
    <span id="span3">Pink Flamingos</span>
    <span id="span4">Female Trouble</span>
    <span id="span5">Desperate Living</span>
    <span id="span6">Polyester</span>
    <span id="span7">Hairspray</span>
    <span id="span8">Cry-Baby</span>
    <span id="span9">Serial Mom</span>

    
    <header>

         <?php
            include('horizontal-nav.html');
        ?>
        
        <div class="banner">
        <h1>Pope of Trash</h1>
         <address>
            Samengesteld door Naomi Beyloos
        </address>
        </div>
        <figure>

            

        </figure>
    </header>
    
    <aside>
        <nav>
            <!-- ol>(li>a)*7 -->
            <ol>
                <li><a href="#span1">John Waters</a></li>
                <li><a href="#span2">Harris Glenn Milstead</a></li>
                <li><a href="#span3">Pink Flamingos</a></li>
                <li><a href="#span4">Female Trouble</a></li>
                <li><a href="#span5">Desperate Living</a></li>
                <li><a href="#span6">Polyester</a></li>
                <li><a href="#span7">Hairspray</a></li>
                <li><a href="#span8">Cry-Baby</a></li>
                <li><a href="#span9">Serial Mom</a></li>


            </ol>
        </nav>
    </aside>
    

    <article>


        <section id="JohnWaters">
            <h2>Het leven van John Waters</h2>

            <figure>
                <img src="images/johnwaters/JohnWaters650x420.jpg" alt="Foto Waters" />
                <figcaption>Foto Waters</figcaption>
            </figure>

            <p>
                John Samuel Waters, Jr. (Baltimore, 22 april 1946) is een Amerikaanse filmregisseur van cultfilms als Pink Flamingos, Hairspray en Cry-Baby. 
                Hij is vooral bekend voor zijn gebruik van camp en choquerende scènes, zijn gewoonte om zijn geboortestad Baltimore als filmlocatie te gebruiken 
                en zijn veelvuldig gebruik van rock-'n-rollmuziek uit de jaren vijftig en jaren zestig. Waters studeerde kortstondig aan New York University maar 
                moest de universiteit verlaten nadat hij betrapt werd tijdens het roken van marihuana. 
                Zijn doorbraak kwam met de film Pink Flamingos in 1972, waarin de hoofdrol gespeeld werd door de travestiet Divine. Daarnaast speelde een aantal 
                acteurs mee die tot Waters vaste acteursstal zouden gaan behoren, de zogenaamde Dreamlanders (naar Waters' productiemaatschappij Dreamland Productions). 
                Pink Flamingos werd berucht door enkele scènes die door velen als zeer schokkend werden ervaren, waaronder de slotscène waarin Divine hondenpoep eet.
                De film werd verboden in onder meer Australië en Noorwegen. Ook zijn twee volgende films, Female Trouble (1974) en Desperate Living (1977), werden door 
                velen als schokkend ervaren. De drie films worden nu gezamenlijk door Waters betiteld als zijn Trash Trilogy. Na Polyester (1981), waarin Divine opnieuw 
                de hoofdrol speelde, begon Waters meer mainstream-films te maken, beginnend met Hairspray waarin Ricki Lake de hoofdrol vervulde, naast onder meer 
                Deborah Harry, Sonny Bono en, wederom, Divine. De film werd in 2002 herschreven tot een met een Tony bekroonde musical, die in 2007 weer verfilmd werd 
                tot Hairspray met onder meer John Travolta. Aan deze film droeg Waters als schrijver bij. 
                De musicalfilm Cry-Baby (1990) met onder meer Johnny Depp, Iggy Pop, Traci Lords en Ricki Lake was Waters' tweede mainstream-film, gevolgd door 
                Serial Mom (1994) met Kathleen Turner als een huisvrouw, die verandert in een seriemoordenaar. 
                John Waters' meest recente film, A Dirty Shame (2004), met Tracey Ullmann, Johnny Knoxville en Selma Blair, is een terugkeer naar de choquerende stijl van 
                zijn vroege films. Waters treedt ook op als (stem)acteur en verteller, zowel in zijn eigen producties als die van anderen, bijvoorbeeld in de televisieseries
                Simpsons en Frasier en in de horrorfilm Seed of Chucky (2004). Ook is hij in verschillende films te zien als zichzelf, bijvoorbeeld in Jackass: Number Two (2004). 
                Hij heeft een aantal boeken geschreven en is ook een beeldend kunstenaar die kunstwerken en installaties gebaseerd op fotografisch materiaal maakt. 
                In 2004 presenteerde het New Museum in New York een overzichtstentoonstelling van zijn werk. Waters' onemanshow This Filthy World, waarmee hij op tournee 
                door de Verenigde Staten ging, werd in 2006 gefilmd en uitgebracht als documentaire.
                
                

            </p>


        </section>



        <section id="HarrisGlennMilstead">

            <h2>Harris Glenn Milstead</h2>

            <figure>
                <img src="images/johnwaters/HarrisGlennMilstead650x366.jpg" alt="Foto Milstead" />
                <figcaption>Foto Milstead</figcaption>
            </figure>
            
            <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/ExL6GxFWpiE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
           </div>
            <p>
                Divine, pseudoniem van Harris Glenn Milstead (Towson, 19 oktober 1945 – Los Angeles, 7 maart 1988), was een Amerikaans acteur, zanger en travestie-artiest. In zijn jeugd raakte Milstead bevriend met leeftijdsgenoot John Waters. Deze liet Milstead in 1966
                voor het eerst in de door hem geregisseerde film Roman Candles optreden als de extravagante travestiet Divine. De naam Divine had Waters ontleend aan het boek Our Lady of the Flowers van Jean Genet. Milstead, alias Divine, speelde in de
                jaren zestig, zeventig en tachtig in meerdere musicals en niet zelden controversiële films, met als een van de bekendste Pink Flamingos uit 1972. Met zijn optredens in theaters en clubs en het uitbrengen van songs kreeg Divine bekendheid
                in de homoscene en in alternatieve kringen. Het Amerikaanse tijdschrift People noemde hem Drag Queen of the Century. De populariteit van Divine waaide korte tijd over naar Europa, waar hij veel aanhang kreeg in Engeland, Duitsland en Nederland.
                De single Shoot Your Shot bereikte in januari 1983 de derde plaats in de Top 40. Ook trad Divine onder meer op in de Amsterdamse homodiscotheek DOK. Milstead wilde laten zien dat hij meer was dan alleen een dragqueen en dat hij ook kon
                acteren in mannelijke rollen. Zo was hij te zien in de films Trouble in Mind (1984) en Hairspray (1988).

            </p>
            
            <figure>
                <img src="images/johnwaters/Divine500x500.jpg" alt="Foto Milstead als Divine" />
                <figcaption>Foto Milstead als Divine</figcaption>
            </figure>

        </section>



        <section id="PinkFlamingos">
            <h2>Pink Flamingos</h2>


            <figure>
                <img src="images/johnwaters/PinkFlamingos650x365.jpg" alt="Foto acteurs Pink Flamingos met Waters" />
                <figcaption>Foto acteurs Pink Flamingos met Waters</figcaption>
            </figure>

            <p>
                Pink Flamingos is een Amerikaanse zwarte komediefilm, geregisseerd, geschreven en geproduceerd door John Waters. Waters nam tevens de montage, cinematografie en muziek voor zijn rekening. De hoofdrollen worden vertolkt door dragqueen Divine, David Lochary,
                en Mary Vivian Pearce, Mink Stole. Pink Flamingos werd geproduceerd met een budget van rond de 10.000 dollar. De opnames vonden vooral plaats in de weekenden in een voorstad van Baltimore. De film leidde bij de originele uitgave tot veel
                controverse en werd een van de beruchtste cultfilms ooit gemaakt, en is tegenwoordig Waters bekendste werk. In 1997 werd de film heruitgebracht vanwege het 25-jarig jubileum. De film staat als 29e op de lijst van 50 Films to See Before
                You Die. De film werd verboden in Australië, enkele Canadese provincies, en Noorwegen. Waters had reeds een script klaar liggen voor een potentieel vervolg getiteld Flamingos Forever. Deze speelt 15 jaar na de gebeurtenissen uit de eerste
                film. De film werd echter nooit gemaakt omdat Divine niet mee wilde werken en Edith Massey in 1984 stierf. Divine woont onder het pseudoniem "Babs Johnson" samen met haar moeder Edie, zoon Crackers en voyeuristische reisgenoot Cotton in
                een woonwagen, gedecoreerd met de roze plastic flamingo’s uit de filmtitel. Wanneer Divine tot “smerigste persoon ooit” wordt verkozen door een krant, besluiten rivalen Connie en Raymond Marble haar en haar familie ten onder te laten gaan.

            </p>

        </section>

        <section id="FemaleTrouble">
            <h2>Female Trouble</h2>

            <figure>
                <img src="images/johnwaters/FemaleTrouble.gif" alt="Gif van Dawn Davenport" />
                <figcaption>Gif van Dawn Davenport</figcaption>
            </figure>

            <p>
                Female Trouble is een Amerikaanse komische misdaadfilm, geregisseerd, geschreven en geproduceerd door John Waters. Met in de hoofdrollen Divine, David Lochary, Mary Vivian Pearce, Mink Stole en Edith Massey. Het verhaal begint wanneer een verwend 
                schoolmeisje (Divine) wegloopt van thuis en zwanger raakt. Uiteindelijk weet ze het te maken tot model wanneer ze opgemerkt wordt door een koppel schoonheidsspecialisten die graag misdaadplegende vrouwen fotograferen.

            </p>
        </section>


        <section id="DesperateLiving">
            <h2>Desperate Living</h2>

            <figure>
                <img src="images/johnwaters/DesperateLiving650x300.jpg" alt="Foto filmposter Desperate Living" />
                <figcaption>Foto filmposter Desperate Living</figcaption>
            </figure>

            <p>
                Desperate Living is een Amerikaanse komische misdaadfilm, geregisseerd, geschreven en geproduceerd door John Waters. Met in de hoofdrollen Liz Renay, Mary Vivian Pearce, Mink Stole, Susan Lowe en Edith Massey. Normaal zou Divine de rol van Susan Low
                spelen maar hij was in een theaterstuk waar hij een langdurig contract had en niet weg kon. Normaal zou David Lochary ook in de film spelen maar hij kwam jammer genoeg te overlijden. Het verhaal start wanneer een neurotische vrouw, van hogere kringen,
                haar man vermoord samen met haar meid. Ze zijn op de vlucht en komen terecht in Mortville. Een gemeenschap van daklozen die geleid wordt door een fascistische koningin.

            </p>

        </section>

        <section id="Polyester">
            <h2>Polyester</h2>

            <figure>
                <img src="images/johnwaters/Polyester650x366.jpg" alt="Foto van scene Polyester" />
                <figcaption>Foto van scene Polyester</figcaption>
            </figure>

            <p>
                Polyester is een Amerikaanse komische film, geregisseerd, geschreven en geproduceerd door John Waters. Met in de hoofdrollen Divine, Tab Hunter, Edith Massey en Mink Stole. Divine en Tab Hunter spelen ook samen in Lust in the Dust. Waters was gevraagd om
                deze film, die Divine en Hunter nog eens herenigd, te regisseren maar hij wees af omdat hij het script niet zelf geschreven had. Omdat Francine haar goede ruikzin centraal staan in de film, kreeg je als je deze film in de bioscoop ging bekijken een geurkaart 
                bij. Je kon met te krabben op de kaart ruiken wat je op het scherm ook zag.
                Francine Fishpaw is een huisvrouw van de hogere middenstand in Baltmore. Ondanks ze een vrome christelijke vrouw is, verdiend haar man zijn brood met zijn theater van +18 films. Waar voor haar buren ook buiten voor haar deur aan het protesteren zijn. 
                Haar zoon is gekend om dames hun voeten te breken door er hard op te trappen en haar dochter is zwanger geraakt. En als of dit nog niet genoeg kopzorgen zijn bedriegt haar man haar ook nog eens met zijn secetaresse.

            </p>
            
            <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/fwtbY9zfOMA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            
        </section>


        <section id="Hairspray">
            <h2>Hairspray</h2>

            <figure>
                <img src="images/johnwaters/hairspray.gif" alt="Gif John Waters" />
                <figcaption>Gif John Waters</figcaption>
            </figure>

            <p>
                Hairspray is een Amerikaanse komische drama film, geregisseerd, geschreven en geproduceerd door John Waters. Met in de hoofdrollen Divine, Sonny Bono, Ruth Brown, Debbie Harry, Ricki Lake en Jerry Stiller. Deze film was trouwens de eerste en de enigste familie
                film van Waters. De droom van Tracy Turnblad komt uit wanneer ze een terugkomende rol krijgt in Corny Collins Dance Show, een zeer populaire dans show voor tieners. Hier door veranderd ze in een tiener idool, deze roem gebruikt ze ook om haar mening tegen. Zij
                is namelijk voor integratie. Hier door krijgt ze echter haar rivaal en voormalige publieks lieveling, Amber Von Tussle, tegen haar omdat Amber en haar ouders tegen integratie zijn. Dit alles loopt hoog op tot het uiteindelijke strijd voor de Miss Auto Show 1963.
                

            </p>
            
            <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/6zgCEgeKoMk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            
        </section>


        <section id="Cry-Baby">
            <h2>Cry-Baby</h2>

            <figure>
                <img src="images/johnwaters/CryBaby650x351.jpg" alt="Foto scene Cry-Baby" />
                <figcaption>Foto scene Cry-Baby</figcaption>
            </figure>

            <p>

                Cry Baby is een muzikale komische film van regisseur John Waters uit 1990, en parodieert in zekere zin de tienermusicals zoals Grease en de Elvis Presley-films. De titelrol wordt gespeeld door Johnny Depp. In de bioscoop was de film geen doorslaand succes,
                maar hij is in de daaropvolgende jaren een cultfilm geworden. De film speelt zich af in 1954. Wade "Cry Baby" Walker is lid van de beruchte groep Drapes, een groep criminelen, volgens de Squares (de brave tegenovergestelde groep van de
                Drapes) tenminste. Op een dag wordt Wade Walker verliefd op Allison Vernon-Williams en ook Allison valt voor de charmes van Cry-Baby. Dit zorgt uiteraard voor veel problemen: het oude liefje van Allison, een square, kan het niet verkroppen
                dat zijn ex met de Drape omgaat. Na een grote rel op een feest van de Drapes wordt Cry-Baby opgesloten tot zijn 21ste. Allison krijgt nu allemaal valse geruchten over Cry-Baby te horen en wil niets met hem meer te maken hebben. Maar daar
                laat Cry-Baby het niet bij.
            </p>
            
            <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/yB1bLlguZVY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            
        </section>

        <section id="SerialMom">
            <h2>Serial Mom</h2>
            
            <div class="youtube">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/OAcimdt8Po0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <p>

                Serial Mom is een Amerikaanse zwarte komedie uit 1994, geregisseerd door John Waters met Kathleen Turner en Ricki Lake in de hoofdrollen. Beverly Suthpin is een perfecte huisvrouw, moeder van twee kinderen en gelukkig getrouwd. Ze lijken het ideale gezin.
                Beverly zorgt ervoor dat het haar kinderen en man aan niets ontbreekt. Ze wonen in de perfecte buurt omringd door nog meer perfecte mensen. Maar schijn bedriegt, in werkelijk is Beverly zo geobsedeerd de ongeschreven burgerlijke regels
                na te streven en houdt ze zo intens veel van haar gezin dat ze alles wat daar een bedreiging voor vormt uit de weg ruimt. De problemen beginnen als Beverly een ouderavond heeft van haar zoon Chip.
            </p>
            
            


        </section>



    </article>

        <footer>
        Gemaakt door Naomi Beyloos
        </footer>
</body>

</html>
